$( document ).ready(function() {
    $('.info-row').hide();
    $('#company-find').show();

    $('#company-find-btn').click(function(){
        $('#company-find').hide();
        $('#company-selected').fadeIn();
        $('#select-matrix').show();
    });

    $('#select-matrix-btn').click(function(){
        $('#select-matrix').hide();
        $('#selected-matrix').fadeIn();
        $('#country-risk').fadeIn();
    });

    $('.sector-matrix-block').click(function(){
        $('#select-matrix').hide();
        $('#selected-matrix').fadeIn();
        $('#country-risk').fadeIn();
    });

    $('.country-risk-btn').click(function(){
        $('#country-risk').hide();
        $('#country-risk-selected').fadeIn();
        $('#shareholders').fadeIn();
    });

    $('.shareholders-btn').click(function(){
        $('#shareholders').hide();
        $('# shareholdes-selected').fadeIn();
        $('#ops-structure').fadeIn();
    });

});